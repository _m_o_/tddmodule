require 'spec_helper'
describe "tddmodule" do
it do
  should contain_file('/myconfigfile').with({
    'ensure' => 'present',
    'content' => /^mysetting$/
  })
end
end
